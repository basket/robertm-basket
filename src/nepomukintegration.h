/***************************************************************************
 *   Copyright (C) 2010 by Amir Pakdel                                     *
 *   pakdel@gmail.com							   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#ifndef NEPOMUKINTEGRATION_H
#define NEPOMUKINTEGRATION_H

#include <QtCore/QThread>
#include <QtCore/QMutex>
#include <QtCore/QTimer>

//For DEBUG_WIN:
#include "global.h"
#include "debugwindow.h"

class QString;
class KUrl;
class BasketScene;

class nepomukIntegration : public QObject
{
    Q_OBJECT
public:
    static void updateMetadata(BasketScene * basket);
    static void deleteMetadata(const QString &fullPath);
    static bool doDelete(const QString &fullPath);
private:
    static nepomukIntegration *instance;
    static QMutex instanceMutex;

    nepomukIntegration(BasketScene * basket, int idleTime);
    ~nepomukIntegration() {
        //I hope deletion is handled automatically
        //delete workerThread;
        //delete cleanupIdle;
        DEBUG_WIN << "nepomukUpdater object destructed";
    }

    int idleTime;
    QThread workerThread;
    QTimer cleanupTimer;
    QMutex mutex;
    QList<BasketScene *> basketList;
    bool isDoingUpdate;
    QList<KUrl> requestedIndexList;
    bool isCleaningupRequestedIndexes;
    void queueBasket(BasketScene * basket);
    void queueIndexRequest(KUrl file);
signals:
    void updateCompleted(QString basketFolderName, bool successful);
    //void indexCleanupCompleted(KUrl file);
private slots:
    void doUpdate();
    void checkQueue();
    void cleanupRequestedIndexes();
    void cleanup();

};

#endif // NEPOMUKINTEGRATION_H
